var TodoModule = angular.module('MyTodoApp', ['ngAnimate']);

TodoModule.controller('TodoController', function ($scope,$http) {

    $scope.toggle=false;
    $scope.viewStatus = 'all';
    $scope.showDelete = false;
    $scope.showDeleteItem = null;
    $scope.selection = [];
    $scope.eEditable= false;
    $scope.eEditable2 = true;
    $scope.editedItem = null;


    $http.get('tasks').
    success(function(data) {
        $scope.tasks = data;
        for(var i = 0;i < $scope.tasks.length;++i)
        {
            if($scope.tasks[i].active) {

                $scope.selection.push($scope.tasks[i].id);
            }
        }

    });


    function update() {
        $scope.selection = [];
        for(var i = 0;i < $scope.tasks.length;++i)
        {
            if($scope.tasks[i].active) {

                $scope.selection.push($scope.tasks[i].id);
            }
        }
    };


    
    $scope.addTask2 = function addTask2() {
        if($scope.taskNaming==""){
            alert("Пустое поле, введите данные");
        }
        else{
            $http.post('tasks/insert/' +'ACTIVE' + '/' + $scope.viewStatus,$scope.taskNaming).
            success(function(data) {
                $scope.tasks = data;
                $scope.taskName="";
                $scope.taskStatus="";
                $scope.taskNaming = "";
                $scope.toggle=!$scope.toggle;
                update();
            }).error(function () {
                alert("Может быть задача с таким именем уже есть, либо иная ошибка")

            });
        }
    };
    


    $scope.toggleSelection = function toggleSelection(taskId) {
        var indexOfCheck = $scope.selection.indexOf(taskId);


        if (indexOfCheck > -1) {
            $http.post('tasks/' +taskId+ '/' + $scope.viewStatus  + '/ACTIVE').
            success(function(data) {
                $scope.tasks = data;
            });
            $scope.selection.splice(indexOfCheck, 1);
        }
            
        else {
            $http.post('tasks/' +taskId+'/' + $scope.viewStatus + '/COMPLETED').
            success(function(data) {
                $scope.tasks = data;
            });
            $scope.selection.push(taskId);
        }
    };


    

    $scope.deleteTask = function (id) {
        $http.post('tasks/delete/' +id + "/"+ $scope.viewStatus).
        success(function(data) {
            $scope.tasks = data;
        });
    }

    $scope.showActiveTasks = function ativeTasks() {
        $http.get("tasks/filter/ACTIVE").success(function (data) {
            $scope.tasks = data;
            update();
        });
    };

    $scope.showCompletedTasks = function CompletedTasks() {
        $http.get("tasks/filter/COMPLETED").success(function (data) {
            $scope.tasks = data;
            update();
        });
    };
    
    $scope.showAllTasks = function AllTasks() {
        $http.get('tasks').
        success(function(data) {
            $scope.tasks = data;
           update();

        });
    };
    
    $scope.editTask = function EditTask(task) {
        $http.post('tasks/edit/'+task.id+  '/' + $scope.viewStatus,task.name).
            success(function (data) {
            $scope.tasks = data;
            update();
            eEditable = 'false';
        });
    };


    $scope.startEditing = function(item){
        item.editing=true;
        $scope.editedItem = item;
    }

    $scope.doneEditing = function(item){
        item.editing=false;
        $scope.editedItem = null;

    }

    $scope.hover = function(task) {
        return $scope.showDeleteItem = task;
    };

    $scope.hover2 = function() {
        return $scope.showDeleteItem = null;
    };

});
