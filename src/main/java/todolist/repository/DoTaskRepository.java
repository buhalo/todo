package todolist.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import todolist.entity.DoTask;

import java.util.List;


public interface DoTaskRepository extends JpaRepository<DoTask,Integer> {

    DoTask findByName(String name);

    List<DoTask> findByActiveTrue();

    List<DoTask> findByActiveFalse();
}
