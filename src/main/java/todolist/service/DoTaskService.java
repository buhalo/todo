package todolist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import todolist.entity.DoTask;
import todolist.repository.DoTaskRepository;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;



@Service
@Transactional
public class DoTaskService {

    @Autowired
    private DoTaskRepository doTaskRepository;


    public void save(DoTask doTask)
    {

        doTaskRepository.save(doTask);
    }

    public void delete(int id)
    {
        doTaskRepository.delete(id);
    }


    public Collection<DoTask> getByStatus(String status)
    {
        switch (status)
        {
            case "completed": return doTaskRepository.findByActiveTrue();
            case "active": return doTaskRepository.findByActiveFalse();
            default: return findAll();
        }
    }

    public Collection<DoTask> findAll()
    {
        return doTaskRepository.findAll();
    }





    public DoTask findById(int id)
    {
        return doTaskRepository.findOne(id);
    }



}
