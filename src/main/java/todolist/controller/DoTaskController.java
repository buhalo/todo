package todolist.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import todolist.entity.DoTask;
import todolist.service.DoTaskService;

import java.util.Collection;


@RestController
public class DoTaskController {

    @Autowired
    DoTaskService service;

    @RequestMapping(value = "/tasks/insert/{active}/{status}",method= RequestMethod.POST)
    public Collection<DoTask> save(@RequestBody String name, @PathVariable String active, @PathVariable String status)
    {

        DoTask doTask = new DoTask();
        doTask.setName(name);
        doTask.setActive(false);
        service.save(doTask);
        System.out.println(name);
        return service.getByStatus(status);
    }

    @RequestMapping(value="/tasks",method = RequestMethod.GET,headers="Accept=application/json")
    public Collection<DoTask> findAll()
    {
        return service.findAll();
    }

    @RequestMapping(value = "/tasks/{id}/{status}/ACTIVE")
    public Collection<DoTask> ActiveCheck(@PathVariable int id,@PathVariable String status)
    {
        DoTask doTask = service.findById(id);
        if(doTask != null)
        {
            doTask.setActive(false);
            service.save(doTask);
        }
        return  service.getByStatus(status);
    }

    @RequestMapping(value = "/tasks/{id}/{status}/COMPLETED")
    public Collection<DoTask> CompletedCheck(@PathVariable int id,@PathVariable String status)
    {
        DoTask doTask = service.findById(id);
        if(doTask != null)
        {
            doTask.setActive(true);
            service.save(doTask);
        }
        return  service.getByStatus(status);
    }

    @RequestMapping(value = "/tasks/edit/{id}/{status}")
    public Collection<DoTask> editTask(@PathVariable int id,@RequestBody String nVal,@PathVariable String status)
    {
        DoTask doTask = service.findById(id);
        doTask.setName(nVal);
        service.save(doTask);
        return service.getByStatus(status);
    }

    @RequestMapping(value = "/tasks/filter/COMPLETED")
    public Collection<DoTask> getCompletedTasks()
    {
        return service.getByStatus("completed");
    }

    @RequestMapping(value = "/tasks/filter/ACTIVE")
    public Collection<DoTask> getActiveTasks()
    {
        return service.getByStatus("active");
    }

    @RequestMapping(value = "/tasks/delete/{id}/{status}")
    public Collection<DoTask> delete(@PathVariable int id,@PathVariable String status)
    {
        service.delete(id);
        return service.getByStatus(status);
    }
}
